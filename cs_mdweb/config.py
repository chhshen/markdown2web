
import yaml

class Config(dict):
    def __init__(self, string):
        super(Config, self).__init__()

        try:
            self.update(yaml.load(string))
        except yaml.YAMLError:
            print('YAML Error')
            raise
        except:
            print('Invalid config format.')
            raise
