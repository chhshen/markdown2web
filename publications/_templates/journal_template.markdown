


Refereed journal papers · {{ num_papers }}
=======================


type|<a href="#">year</a>|<a href="#">title</a>|<a href="#">venue</a>|<a href="#">authors</a>
-----|-----|-----|-----|-----:
{% for t in type -%}
{%  if t|lower == 'article'       -%}
<a data-toggle="tooltip" data-placement="left" title="journal paper" href="#"><i class="icon-fixed-width dg icon-file"></i></a>| {{ year[loop.index-1]|replace('20','&rsquo;') }} |  <em><a href="{{ year[loop.index-1] }}.html#{{ key[loop.index-1] }}"> {{ title[loop.index-1] |   replace('{','')|replace('}','') }}  </a></em>  |<a data-toggle="tooltip" data-placement="top" title="{{ journal[loop.index-1] }}" href="#">{{  venue[loop.index-1] }}</a>| {{  authors[loop.index-1]|replace('{','')|replace('}','')  }}
{% endif  -%}
{% endfor -%}

