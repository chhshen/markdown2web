

Refereed journal papers
=======================

type|year|title|venue|authors
-----|-----|-----|-----|-----:
{% for t in type -%}
{%  if t|lower == 'article' -%}
<a data-toggle="tooltip" data-placement="left" title="journal article" href="#"><i class="icon-fixed-width dg icon-file"></i></a>| {{ year[loop.index-1]|replace('20','&rsquo;') }} |   {{ title[loop.index-1] |   replace('{','')|replace('}','') }} |<a data-toggle="tooltip" data-placement="top" title="{{ journal[loop.index-1] }}" href="#">{{  venue[loop.index-1] }}</a>| {{  authors[loop.index-1]|replace('{','')|replace('}','')  }}
{% endif -%}
{% endfor %}


<!-- comment -->


Refereed conference papers
==========================

type|year|title|venue|authors
-----|-----|-----|-----|-----:
{% for t in type -%}
{%  if t|lower == 'inproceedings' -%}
<a data-toggle="tooltip" data-placement="left" title="conference paper" href="#"><i class="icon-fixed-width db icon-file-alt"></i></a> | {{ year[loop.index-1]|replace('20','&rsquo;') }} |   {{ title[loop.index-1] |   replace('{','')|replace('}','') }} |<a data-toggle="tooltip" data-placement="top" title="{{ booktitle[loop.index-1] }}" href="#">{{  venue[loop.index-1] }}</a>| {{  authors[loop.index-1]|replace('{','')|replace('}','')  }}
{% endif -%}
{% endfor %}


   
<!-- comment -->



Other papers
============

type|year|title|venue|authors
-----|-----|-----|-----|-----:
{% for t in type -%}
{%  if not t|lower in ['inproceedings','article'] -%}
<a data-toggle="tooltip" data-placement="top" title="book, book chapter, technical report or unpublished work" href="#"><i class="icon-fixed-width red icon-file-text"></i></a> | {{ year[loop.index-1]|replace('20','&rsquo;') }} |   {{ title[loop.index-1] |   replace('{','')|replace('}','') }} |<a data-toggle="tooltip" data-placement="top" title="{{ publisher[loop.index-1] }}" href="#">{{  venue[loop.index-1] }}</a>| {{  authors[loop.index-1]|replace('{','')|replace('}','')  }}
{% endif -%}
{% endfor -%}


