


Selected other papers 
=======================


type|year|title|venue|authors
-----|-----|-----|-----|-----:
{% for t in type -%}
{%  if not t|lower in ['inproceedings','article']  and   venue[loop.index-1] |lower in  selected_venues | lower | str2list -%}
<a data-toggle="tooltip" data-placement="top" title="book, book chapter, technical report or unpublished work" href="#"><i class="icon-fixed-width red icon-file-alt"></i></a> | {{ year[loop.index-1]|replace('20','&rsquo;') }} |  <em><a href="{{ year[loop.index-1] }}.html#{{ key[loop.index-1] }}"> {{ title[loop.index-1] |   replace('{','')|replace('}','') }}  </a></em> |<a data-toggle="tooltip" data-placement="top" title="{{ publisher[loop.index-1] }}" href="#">{{  venue[loop.index-1] }}</a>| {{  authors[loop.index-1]|replace('{','')|replace('}','')  }}
{% endif -%}
{% endfor -%}


