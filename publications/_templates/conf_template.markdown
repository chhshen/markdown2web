


Refereed conference papers · {{ num_papers }}
=======================




type|year|title|venue|authors
-----|-----|-----|-----|-----:
{% for t in type -%}
{%  if t|lower == 'inproceedings' -%}
<a data-toggle="tooltip" data-placement="left" title="conference paper" href="#"><i class="icon-fixed-width db icon-file-text"></i></a> | {{ year[loop.index-1]|replace('20','&rsquo;') }} |   <em><a href="{{ year[loop.index-1] }}.html#{{ key[loop.index-1] }}">{{ title[loop.index-1] |   replace('{','')|replace('}','') }}</a></em> |<a data-toggle="tooltip" data-placement="top" title="{{ booktitle[loop.index-1] }}" href="#">{{  venue[loop.index-1] }}</a>| {{  authors[loop.index-1]|replace('{','')|replace('}','')  }}
{% endif -%}
{% endfor -%}


