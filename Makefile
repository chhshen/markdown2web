#
# makefile for csweb
# Chunhua Shen, Aug. 2013
#

ODIR=./htmls


.PHONY: clean

all:  dist tidy clean  tar


build:
	./build.sh

# call html-tidy
tidy:
	./build.sh $(ODIR)

clean:
	rm -f *html
	rm -f publications/*html
	rm -f publications/paper*md
	rm -f publications/*bib
	rm -f publications/$(ODIR)/*

dist: build
	mkdir -p $(ODIR)
	rm -fr $(ODIR)/*
	cp -r _assets $(ODIR)
	for f in `ls -1 *html`; \
	do \
    	cp $$f   $(ODIR); \
	done
	echo "."
	echo "The output files for your website are in: $(ODIR)."

tar: dist
	tar zcvf htmls.tar.gz  $(ODIR)

purge: clean
	rm -fr $(ODIR)
	rm -fr publications/year/
	rm -fr publications/output
	rm -f htmls.tar*


