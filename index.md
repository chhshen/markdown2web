# Dr. A Bcde


[XXXX XXXX](http://www.xxx.gov.au/ncgp/futurefel/future_default.htm) 
(Step X, awarded in 2100) and [Xxxx Xxxx](http://en.wikipedia.org/wiki/Academic_ranks_%28Australia_and_New_Zealand%29)
at School of XXXXX Science, [XXXX of XXXX](http://www.xxxxxxxxx.edu.au);
also a member of the [Australian Centre for XXXX XXXX (XXXX)](http://www.xxxxxxxxx.com.au). 
My research and teaching focus on [Statistical Machine Learning](http://en.wikipedia.org/wiki/Machine_learning)
and [XXXXX Vision](http://en.wikipedia.org/wiki/XXXXX_vision).

Before moving back to [xxxxxxx](http://www.southaustralia.com/) in 2011, 
I was with the XXXXX vision group at [XXXXXX
(formerly XXXX XXX XXXX)](http://www.xxxxxx.com.au) XXXXXXX Research Laboratory. I held an
adjunct position at College of Engineering & XXXXX Science, [XXXXX
XXXXXX XXXX](http://www.xxxx.edu.au) from 200X to 201X.


Places having been affiliated with:

* XXXX of XXXXXX
* XXXX, XXXX Research Laboratory
* [XXXX XXXX](http://xxxx.org.au) (XXXX)
* XXX XXXX XXXX, College of Engineering & XXXXX Science




<div id="Collapsible_bio">
<a href="#bio" data-toggle="collapse"> 
    <i class="icon-fixed-width  icon-collapse"></i>A formal, brief biography</a>
</div>

<blockquote id="bio" class="collapse">
XXX XXX is a senior lecturer at School of XXXXX Science, 
XXXX of XXXX. Prior to that, he was with the XXXXX vision
program at XXXX (XXXXXXX XX XXX), XXXX Research
Laboratory for about XXX years. His research interests are in the
intersection of XXXXX vision and statistical machine learning.
Recent work has been on real-time object detection, large-scale image
retrieval and classification, and scalable nonlinear optimization.
<br>
He studied at XXX XXXX, at XX XXX XX University,
and received his PhD degree from the XXXX of XXXX. From 201X
to 20XX, he holds an XX XXX XXX XXX Fellowship.
</blockquote>




# Group members

I am advising a few PhD students and postdoc researchers:

## Graduate students

--------------                                                              -------------------------------------
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>Xxxxx XXX        [xxxxxxx](http://www.xxxxxxx.edu.au)
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XXX              [xxxxxxx](http://www.xxxxxxx.edu.au)
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XXX XXX          [xxxxxxx](http://www.xxxxxxx.edu.au)
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XXX XXXXX        [xxxxxxx](http://www.xxxxxxx.edu.au)
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XXX XXX          [xxxxxxx](http://www.xxxxxxx.edu.au)
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XXX XXX          [xxxxxxx](http://www.xxxxxxx.edu.au); co-supervised with Prof. XXX
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>Xxxxxxxxg X      [xxxxxxx](http://www.xxxxxxx.edu.au); co-supervised with Prof. XX XX
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XXX XX           [xxxxxxx](http://www.xxxxxxx.edu.au)
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XX XX            [ZZZ](http://www.xxx.edu.au) and ZZZZ; co-supervised with Prof. XX XX
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XXX XX           [ZZZ](http://www.xxx.edu.au); co-supervised with Dr. XX XX
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XX XX            visiting from ZZZ XXXX of Technology
--------------                                                              -------------------------------------



## Postdoc researchers

--------------------------------                                                            -------------------------------------------------------
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XXXX XXXX                          project: XXX 
<i class="icon-fixed-width dg icon-chevron-sign-right"></i>XXXXX  XXXX                        project: XX
--------------------------------                                                            -------------------------------------------------------


## Former PhD students

--------------------------------                                                         ------------------------------------
<i class="icon-fixed-width dg icon-circle-arrow-left"></i>XXXX XXXX                      Completed 2011, ZZZ and ZZZZ;  now at [Xxxxx](http://www.Xxxxx.gov.au)
<i class="icon-fixed-width dg icon-circle-arrow-left"></i>XXX XXXX                       Completed 2011,  ZZZ and ZZZZ; now at [xxxxx](http://www.xxxxx.com.au)
<i class="icon-fixed-width dg icon-circle-arrow-left"></i>XXXX XXX XXXX XXXX             Completed 2010, XXXX and ZZZZ; now at [xxxxx](http://www.xxxxx.com.au)
<i class="icon-fixed-width dg icon-circle-arrow-left"></i> XX                            Completed 20 [XXXX of Xxxxx](http://www.xxxxx.edu.au)
<i class="icon-fixed-width dg icon-circle-arrow-left"></i>XXX XXX X                      visited from ZZZg XXXX, 2008→2010; now at xxxxx
<i class="icon-fixed-width dg icon-circle-arrow-left"></i>XXX XXX                        visited from Xogy (XXXX), 2009→2010; now at XXXX
<i class="icon-fixed-width dg icon-circle-arrow-left"></i>XX XXXXX                       visited from ZZZ XXXX of Technology, 2010→2011, now at XXXX
<i class="icon-fixed-width dg icon-circle-arrow-left"></i>XXX XX xxxxxxx                 visited from XXXX XXXX of Science and Technology, 2010→2012
<i class="icon-fixed-width dg icon-circle-arrow-left"></i>XX XXXX                        visited from, 2011→2013; now at XX
--------------------------------                                                         -------------------------------------



# Contact

-----------------                                                -------
<i class="icon-fixed-width dg icon-envelope"></i>postal address  School of XXXXX Science, XXXX of xxxxxxx, xxxxxxx, XX X00X, Australia
<i class="icon-fixed-width dg icon-home"></i>office address      Level 5, XXXXXX, XXXX of xxxxxxx <a href="http://goo.gl/XuwMcTxx" data-toggle="tooltip" data-placement="right" title="Check the address on Google map"><i class="icon-fixed-width icon-map-marker"></i>Google map</a> 
<i class="icon-fixed-width dg icon-print"></i>facsimile          $+61 ~(0)8 ~ZZZZ ~4366$
<i class="icon-fixed-width dg icon-phone"></i>telephone          $+61 ~(0)8 ~ZZZZ ~6745$
<i class="icon-fixed-width dg icon-envelope-alt"></i>e-mail      xxxxxxx.xxxxxxx$@$xxxxxxx.edu.au
<i class="icon-fixed-width dg icon-desktop"></i>www              [http://cs.xxxxxxx.edu.au/~chhxxxxxxx](http://cs.xxxxxxx.edu.au/~chhxxxxxxx)
<i class="icon-fixed-width dg icon-skype"></i>skype              chhxxxxxxx$@$xxxxx.com
<i class="icon-fixed-width dg icon-twitter"></i>twitter          [http://twitter.com/chhxxxxxxx](https://twitter.com/chhxxxxxxx)
-----------------                                                -------



